class Walker
{
  float xPosition;
  float yPosition;
  
  
  void render()
  {
    circle(xPosition, yPosition, 30);
  }
  
  void randColor()
  {
    int a = floor(random(256));
    int b = floor(random(256));
    int c = floor(random(256));
    color shade = color(a, b, c, 80);
    fill(shade);
  }
  
  void randomWalk()
  {
    int decision = floor(random(8));
    int randStep = floor(random(10));
  
    switch(decision)
    {
      case 0: //Forward
        yPosition += randStep;
        break;
      case 1: //Backward
        yPosition -= randStep;
        break;
      case 2: //Left
        xPosition -= randStep;
        break;
      case 3: //Right
        xPosition += randStep;
        break;
      case 4: //UpLeft
        yPosition += randStep;
        xPosition -= randStep;
        break;
      case 5: //UpRight
        yPosition += randStep;
        xPosition += randStep;
        break;
      case 6: //DownLeft
        yPosition -= randStep;
        xPosition -= randStep;
        break;
      case 7: //DownRight
        yPosition -= randStep;
        xPosition += randStep;
        break;
    }
    
    println(randStep);
  }
}
