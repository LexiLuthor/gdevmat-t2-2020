public class Walker
{
  public Vector2 position = new Vector2();
  public Vector2 velocity = new Vector2(10, 12);

  void render()
  {
    circle(position.x, position.y, 100);
  }
  
  void randColor()
  {
    int a = floor(random(256));
    int b = floor(random(256));
    int c = floor(random(256));
    color shade = color(a, b, c, 80);
    fill(shade);
  }
  
  void bounce()
  {  
    position.add(velocity);
  
    if (position.x > Window.right || position.x < Window.left)
    {
      velocity.x *= -1;
    }
    
    if (position.y > Window.top || position.y < Window.bottom)
    {
      velocity.y *= -1;
    }
  }
}
