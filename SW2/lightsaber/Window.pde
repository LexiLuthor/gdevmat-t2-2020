public static class Window
{
  public static int WidthPx = 1920;
  public static int HeightPx = 1080;
  public static int windowWidth = WidthPx/ 2;
  public static int windowHeight = HeightPx/ 2;
  public static int top = windowHeight;
  public static int bottom = -windowHeight;
  public static int right = windowWidth;
  public static int left = -windowWidth;
  
}
